import React, {Component} from 'react';
import {StyleSheet, Alert, FlatList, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {ThemeManager, Colors, BorderRadiuses, ListItem, Text} from 'react-native-ui-lib'; //eslint-disable-line
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialIcons';

export default class Menu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            onEdit: false,
            updating: false,
        };
    }

    menuItems = [
        {
            name:'TodayScreen',
            title:'לוח יומי',
            icon:(<Icon name='time-slot' size={30} color="#000" />)
        },
        {
            name:'ServicesScreen',
            title:'שירותים',
            icon:(<Icon2 name='list-alt' size={30} color="#000" />)
        },
        {
            name:'EmployeesScreen',
            title:'צוות',
            icon:(<Icon3 name='group' size={30} color="#000" />)
        },
        {
            name:'CustomersScreen',
            title:'לקוחות',
            icon:(<Icon name='users' size={30} color="#000" />)
        },
    ];

    keyExtractor = (item, index) => Math.random().toString();

    renderRow(screenDetail, id) {
        const statusColor = Colors.green30;
        return (
            <ListItem
                containerStyle={[styles.border]}
                activeBackgroundColor={Colors.dark60}
                activeOpacity={0.3}
                height={77.5}
                onPress={() => this.props.navigate(screenDetail.name)}
                animation="fadeIn"
                easing="ease-out-expo"
                duration={1000}
                useNativeDriver>
                <ListItem.Part middle column containerStyle={[{paddingLeft: 5}]}>
                    <ListItem.Part containerStyle={{marginBottom: 3,flexDirection:'row-reverse',justifyContent:'flex-start'}}>

                        <View style={{width: 62,marginRight: 30 ,alignItems:'flex-end'}}>
                            {screenDetail.icon}
                        </View>
                        <View>
                            <View>
                                <Text
                                    dark10
                                    text70
                                    style={{marginTop: 2}}>{screenDetail.title}</Text>
                            </View>
                        </View>
                    </ListItem.Part>
                </ListItem.Part>
            </ListItem>
        );
    }


    render() {
        return (
            <View style={{flex:1}}>
                <FlatList
                    data={this.menuItems}
                    renderItem={({item, index}) => this.renderRow(item, index)}
                    keyExtractor={this.keyExtractor}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 24,
        height: 24,
        borderRadius: BorderRadiuses.br20,
        marginHorizontal: 14,
        marginLeft: 0
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
