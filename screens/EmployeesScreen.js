import React, {Component} from 'react';
import {StyleSheet, Alert, FlatList, View, StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {ThemeManager, Colors, BorderRadiuses, ListItem, Text} from 'react-native-ui-lib'; //eslint-disable-line
import users from '../data/users';
import {Toolbar} from "react-native-material-ui";
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class EmployeesScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'עובדים',
    };

    constructor(props) {
        super(props);

        this.state = {
            onEdit: false,
            updating: false,
        };
    }

    keyExtractor = (item, index) => item.display_name;

    renderRow(user, index) {
        const statusColor = Colors.green30;
        const {navigation} = this.props;
        const cellIcon = (<Icon name="call" size={30} color="#000" />);


        return (
            <ListItem
                activeBackgroundColor={Colors.dark60}
                activeOpacity={0.3}
                height={77.5}
                animation="fadeIn"
                easing="ease-out-expo"
                duration={1000}
                onPress={() => navigation.navigate('EmployeeScreen', {
                    user,
                    index,

                })}
                style={{
                    flexDirection: 'row-reverse'
                }}
                useNativeDriver>
                <ListItem.Part middle column
                               containerStyle={[
                                   styles.border,
                                   {
                                       paddingLeft: 17,
                                   }
                               ]}>
                    <ListItem.Part containerStyle={{
                        flexDirection: 'row-reverse',
                        marginBottom: 3
                    }}>
                        <View>
                            <Animatable.Image
                                source={{uri: 'https://conceptdraw.com/a459c4/p8/preview/640/pict--young-man-ivr-people-vector-stencils-library'}}
                                style={styles.image}
                                animation="fadeInLeft"
                                easing="ease-out-expo"
                                duration={600}
                                delay={120}
                                useNativeDriver/>
                        </View>
                        <View style={{
                            flex: 1
                        }}>
                            <View>
                                <Text
                                    text70
                                    style={{marginTop: 2, color: '#3B4954'}}>{user.display_name}</Text>
                            </View>
                            <View>
                                <Text text90
                                      color={statusColor}
                                      numberOfLines={1}
                                      style={{
                                          textAlign: 'right',
                                          writingDirection: 'rtl'
                                      }}>{`${user.schedule.time} ${user.schedule.service.display_name} ל${user.schedule.customer.display_name}`}</Text>
                            </View>

                        </View>
                    </ListItem.Part>
                </ListItem.Part>

                <ListItem.Part>
                    {cellIcon}
                </ListItem.Part>
                <ListItem.Part>
                    <Animatable.Image
                        source={{uri: 'http://icons.iconarchive.com/icons/dtafalonso/android-l/256/WhatsApp-icon.png'}}
                        style={styles.imageCallerIcons}
                        animation="fadeInLeft"
                        easing="ease-out-expo"
                        duration={600}
                        delay={120}
                        useNativeDriver/>
                </ListItem.Part>
            </ListItem>
        );
    }

    onRightElementPress = ({action, index}) => {
        console.log(action, index);
    }


    render() {
        return (
            <View>
                <StatusBar
                    backgroundColor="#9CABBA" // #3B4954
                    barStyle="light-content"
                />
                <Toolbar
                    style={{
                        rightElement: {
                            color: '#fff'
                        },
                        titleText: {
                            color: '#fff'
                        },
                        container: {
                            elevation: 0,
                            backgroundColor: '#9CABBA',
                            shadowColor: 'transparent',
                            flexDirection: 'row-reverse'
                        }
                    }}
                    leftElement="menu"
                    centerElement="צוות"
                    onLeftElementPress={() => this.props.navigation.openDrawer()}
                    searchable={{
                        autoFocus: true,
                        placeholder: 'חפש',
                    }}
                    onRightElementPress={this.onRightElementPress}
                    rightElement={{
                        actions: ['add'],
                        // menu: { labels: ['Item 1', 'Item 2'] },
                    }}
                />
                <FlatList
                    data={users}
                    renderItem={({item, index}) => this.renderRow(item, index)}
                    keyExtractor={this.keyExtractor}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    imageCallerIcons: {
        width: 24,
        height: 24,
        borderRadius: BorderRadiuses.br20,
        marginHorizontal: 14,
    },
    image: {
        width: 44,
        height: 44,
        borderRadius: BorderRadiuses.br20,
        marginHorizontal: 14,
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
