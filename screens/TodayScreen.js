import React, {Component} from 'react';
import {StyleSheet, Alert, FlatList, View, StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {ThemeManager, Colors, BorderRadiuses, ListItem, Text} from 'react-native-ui-lib'; //eslint-disable-line
import users from '../data/users';
import {Toolbar, BottomNavigation} from "react-native-material-ui";

export default class CustomersScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'המשימות של היום',
    };

    constructor(props) {
        super(props);

        this.state = {
            onEdit: false,
            updating: false,
        };
    }

    keyExtractor = (item, index) => item.display_name;


    renderRow(user, id) {
        const statusColor = Colors.green30;
        return (
            <ListItem
                containerStyle={[styles.border, {paddingLeft: 10}]}
                style={{flexDirection: 'row-reverse', flex: 1}}
                activeBackgroundColor={Colors.dark60}
                activeOpacity={0.3}
                height={77.5}
                onPress={() => Alert.alert(`pressed on order #${id + 1}`)}
                animation="fadeIn"
                easing="ease-out-expo"
                duration={1000}
                useNativeDriver>
                <ListItem.Part containerStyle={{marginRight: 'auto'}}>
                    <ListItem.Part containerStyle={{marginBottom: 3, flexDirection: 'row-reverse'}}>
                        <View style={{
                            width: 62,
                            borderLeftWidth: 1,
                            borderColor: '#52A3E3',
                            paddingLeft: 10,
                            marginLeft: 10
                        }}>
                            <View>
                                <Text
                                >12:00</Text>
                            </View>
                            <View>
                                <Text
                                    dark50
                                >13:30</Text>
                            </View>
                        </View>
                        <View>
                            <View>
                                <Text
                                    dark10
                                    text70
                                    style={{marginTop: 2}}>{user.display_name}</Text>
                            </View>
                            <View>
                                <Text
                                    dark50
                                    text90
                                    style={{marginTop: 2}}>{`${user.schedule.customer.display_name} ל-${user.schedule.service.display_name}`}</Text>
                            </View>
                        </View>
                    </ListItem.Part>
                </ListItem.Part>

                <ListItem.Part>
                    <Animatable.Image
                        source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5'}}
                        style={styles.image}
                        animation="fadeInLeft"
                        easing="ease-out-expo"
                        duration={600}
                        delay={70}
                        useNativeDriver/>
                </ListItem.Part>
                <ListItem.Part>
                    <Animatable.Image
                        source={{uri: 'http://icons.iconarchive.com/icons/dtafalonso/android-l/256/WhatsApp-icon.png'}}
                        style={styles.image}
                        animation="fadeInLeft"
                        easing="ease-out-expo"
                        duration={600}
                        delay={120}
                        useNativeDriver/>
                </ListItem.Part>
            </ListItem>
        );
    }


    render() {
        return (
            <View style={{flex: 1}}>
                <StatusBar
                    backgroundColor="#9CABBA" //#3B4954
                    barStyle="light-content"
                />

                <Toolbar
                    style={{
                        leftElement: {
                            color: '#fff'
                        },
                        rightElement: {
                            color: '#fff'
                        },
                        titleText: {
                            color: '#fff'
                        },
                        container: {
                            elevation: 0,
                            backgroundColor: '#9CABBA',
                            shadowColor: 'transparent',
                            flexDirection:'row-reverse'
                        }
                    }}
                    leftElement="menu"
                    centerElement="לוח יומי"
                    onLeftElementPress={() => this.props.navigation.openDrawer()}
                    searchable={{
                        autoFocus: true,
                        placeholder: 'חיפוש',
                    }}
                />
                <FlatList
                    data={users}
                    renderItem={({item, index}) => this.renderRow(item, index)}
                    keyExtractor={this.keyExtractor}
                />

                {/*<BottomNavigation active={this.state.active} hidden={false} >*/}
                {/*<BottomNavigation.Action*/}
                {/*key="today"*/}
                {/*icon="today"*/}
                {/*label="Today"*/}
                {/*onPress={() => this.setState({ active: 'today' })}*/}
                {/*/>*/}
                {/*<BottomNavigation.Action*/}
                {/*key="people"*/}
                {/*icon="people"*/}
                {/*label="People"*/}
                {/*onPress={() => this.setState({ active: 'people' })}*/}
                {/*/>*/}
                {/*<BottomNavigation.Action*/}
                {/*key="bookmark-border"*/}
                {/*icon="bookmark-border"*/}
                {/*label="Bookmark"*/}
                {/*onPress={() => this.setState({ active: 'bookmark-border' })}*/}
                {/*/>*/}
                {/*<BottomNavigation.Action*/}
                {/*key="settings"*/}
                {/*icon="settings"*/}
                {/*label="Settings"*/}
                {/*onPress={() => this.setState({ active: 'settings' })}*/}
                {/*/>*/}
                {/*</BottomNavigation>*/}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 24,
        height: 24,
        borderRadius: BorderRadiuses.br20,
        marginHorizontal: 14,
        marginLeft: 0
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
