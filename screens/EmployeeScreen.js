import React, {Component} from 'react';
import {View, TabBar, Text, TextInput, Card, TextArea, Picker,ThemeManager, Colors, } from 'react-native-ui-lib';
import AgendaScreenWix from "../components/calendar";
import ToggleDisplay from "../components/ToggleDisplay";
import {ScrollView, StatusBar, StyleSheet} from 'react-native';
import {Toolbar} from "react-native-material-ui";
import * as Animatable from "react-native-animatable";
import {Dropdown} from 'react-native-material-dropdown';

const cardImage = require('../assets/man-312366_960_720.png');

let types = [
    {value: 'טלפון'},
    {value: 'בית'},
    {value: 'עבודה'},
    {value: 'אחר'},
];


export default class EmployeeScreen extends Component {

    static navigationOptions = {
        drawerLabel: 'עובדים',
    };

    constructor(props) {
        super(props);
        this.state = {
            showTabs: 'Details'
        }
    }

    onRightElementPress = ({action,index})=>{
        console.log(action,index);
    }



    render() {
        const {navigation} = this.props;
        const index = navigation.getParam('index', null);
        const employee = navigation.getParam('employee', null);
        const rightElementActions = employee ? ['delete'] : [];

        return (
            <View flex paddingT-0>
                <StatusBar
                    backgroundColor={Colors.green30} // #3B4954
                    barStyle="light-content"
                />
                <Toolbar
                    style={{
                        leftElement: {
                            color: '#fff'
                        },
                        rightElement: {
                            color: '#fff'
                        },
                        titleText: {
                            color: '#fff'
                        },
                        container: {
                            elevation: 0,
                            shadowOpacity: 0,
                            backgroundColor: Colors.green30,
                            flexDirection: 'row-reverse'
                        },
                        theme: {
                            boxShadow: 'none',
                            shadowOpacity: 0
                        }

                    }}
                    containerStyle={{
                        boxShadow: 'none',
                        elevation: 0,
                        shadowOpacity: 0
                    }}
                    leftElement="arrow-forward"
                    onLeftElementPress={() => this.props.navigation.goBack(null)}
                    centerElement="צוות"
                    onRightElementPress={this.onRightElementPress}
                    rightElement={{
                        actions: rightElementActions,
                        // menu: { labels: ['Item 1', 'Item 2'] },
                    }}
                />

                <Card row
                      enableShadow={false}
                      height={100}
                      style={{
                          marginBottom: 0,
                          borderRadius:0,
                          backgroundColor: Colors.green30,
                      }}
                      onPress={() => {
                      }}

                      enableBlur>
                    <Card.Section body style={{
                    }}>
                        <Card.Section style={{
                            marginBottom: 25,
                            flexDirection:'row-reverse'
                        }}>
                            <Text text40 dark10 style={{
                                color: 'white',
                            }}>
                                אבי
                            </Text>
                        </Card.Section>
                        <Card.Section footer style={{
                            flexDirection:'row-reverse'
                        }}>
                            <Text text90 style={{color:'#3B4954'}}>
                                חופף,צבע,קצוות מפוצלים
                            </Text>
                        </Card.Section>
                    </Card.Section>
                    <Card.Section body style={{
                        flexDirection:'row-reverse',
                        flexGrow: 0,
                    }}>
                        <Animatable.Image
                            source={cardImage}
                            style={styles.image}
                            animation="fadeInLeft"
                            easing="ease-out-expo"
                            duration={600}
                            delay={70}
                            useNativeDriver/>
                    </Card.Section>

                </Card>




                <TabBar selectedIndex={2}>
                    <TabBar.Item onPress={() => {
                        this.setState({showTabs: 'Account'})
                    }}>
                        <Text>חשבון</Text>
                    </TabBar.Item>
                    <TabBar.Item onPress={() => {
                        this.setState({showTabs: 'Availability'})
                    }}>
                        <Text>זמינות</Text>
                    </TabBar.Item>
                    <TabBar.Item onPress={() => {
                        this.setState({showTabs: 'Details'})
                    }}>
                        <Text>פרטים</Text>
                    </TabBar.Item>

                </TabBar>


                <ToggleDisplay hide={this.state.showTabs !== 'Details'}
                               style={{flex: 1, paddingLeft: 20, paddingRight: 20, paddingBottom: 30}}>
                    <ScrollView showsVerticalScrollIndicator={false} >
                        <Text style={{marginTop: 20, fontWeight: '700'}} text70>
                            שם
                        </Text>
                        <TextInput
                            floatingPlaceholder
                            placeholder="שם"
                            titleStyle={{fontSize: 15}}
                        />

                        <Text style={{fontWeight: '700'}} text70>
                            טלפון
                        </Text>

                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row'
                        }}>
                            <TextInput
                                style={{width: '60%'}}
                                floatingPlaceholder
                                placeholder=" טלפון"
                                titleStyle={{fontSize: 15}}
                            />

                            <View
                                style={{width: '30%', marginLeft: '10%', paddingBottom:11}}>
                                <Dropdown
                                    label='תווית'
                                    data={types}
                                />
                            </View>
                        </View>

                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row'
                        }}>
                            <TextInput
                                style={{width: '60%'}}
                                floatingPlaceholder
                                placeholder=" טלפון"
                                titleStyle={{fontSize: 15}}
                            />
                            <View
                                style={{width: '30%', marginLeft: '10%', paddingBottom:11}}>
                                <Dropdown
                                    label='תווית'
                                    data={types}
                                />
                            </View>
                        </View>

                        <Text style={{fontWeight: '700'}} text70>
                            דוא״ל
                        </Text>
                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row'
                        }}>
                            <TextInput
                                style={{width: '60%'}}
                                floatingPlaceholder
                                placeholder="דוא״ל"
                                titleStyle={{fontSize: 15}}
                            />

                            <View
                                style={{width: '30%', marginLeft: '10%', paddingBottom:11}}>
                                <Dropdown
                                    label='תווית'
                                    data={types}
                                />
                            </View>
                        </View>

                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row'
                        }}>
                            <TextInput
                                style={{width: '60%'}}
                                floatingPlaceholder
                                placeholder="דוא״ל"
                                titleStyle={{fontSize: 15}}
                            />
                            <View
                                style={{width: '30%', marginLeft: '10%', paddingBottom:11}}>
                                <Dropdown
                                    label='תווית'
                                    data={types}
                                />
                            </View>
                        </View>


                        <Text style={{fontWeight: '700'}} text70>
                            הערות
                        </Text>
                        <View
                            style={{
                                height: 150,
                                borderWidth: 1,
                                marginBottom: 10,
                                padding: 10,
                                borderColor: '#679',
                            }}>
                            <TextArea placeholder="כתוב הערות"/>
                        </View>


                    </ScrollView>
                </ToggleDisplay>


                <ToggleDisplay hide={this.state.showTabs !== 'Availability'} style={{flex: 1}}>
                    <AgendaScreenWix/>
                </ToggleDisplay>

                <ToggleDisplay hide={this.state.showTabs !== 'Account'}>
                    <Text>תוכן חשבון</Text>
                </ToggleDisplay>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 70,
        height: 70,
        borderRadius: 50,
        marginLeft: 0
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
