import React, {Component} from 'react';
import {ScrollView, StatusBar, StyleSheet} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {View, TabBar, Text, Card, ThemeManager, BorderRadiuses, TextInput} from 'react-native-ui-lib';
import {Toolbar, Button, COLOR, Checkbox, Dialog, DialogDefaultActions} from 'react-native-material-ui';
import ToggleDisplay from "../components/ToggleDisplay";
import {Dropdown} from 'react-native-material-dropdown';

export default class ServiceScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'שירות',
    };


    constructor(props) {
        super(props);

        this.state = {
            currentTab: 'Details'
        }
    }

    onRightElementPress = ({action, index}) => {
        if (action === 'delete') {

        }
    }

    render() {

        const styles = StyleSheet.create({
            container: {
                paddingTop: 16,
            },
        });


        const {navigation} = this.props;
        const index = navigation.getParam('index', null);
        const service = navigation.getParam('service', null);

        let timeUnits = [{
            value: 'דקות',
        }, {
            value: 'שעות',
        }];

        let timeCount = [
            {value: 10},
            {value: 20},
            {value: 30},
            {value: 40},
            {value: 50},
            {value: 60},
        ];


        const rightElementActions = service ? ['delete'] : [];
        return (
            <View  style={{backgroundColor:'#fff'}} flex>
                {/*<View style={{*/}
                {/*height: '100%',*/}
                {/*flexDirection:'column',*/}
                {/*alignItems: 'center',*/}
                {/*justifyContent: 'center',*/}
                {/*}}>*/}
                {/*<View>*/}
                {/*<Dialog>*/}
                {/*<Dialog.Title>*/}
                {/*<Text>אתה בטוח שאת\ה רוצה למחוק משימה ?</Text>*/}
                {/*</Dialog.Title>*/}
                {/*<Dialog.Content>*/}
                {/*<Text>*/}
                {/*שם המשימה*/}
                {/*</Text>*/}
                {/*</Dialog.Content>*/}
                {/*<Dialog.Actions>*/}
                {/*<DialogDefaultActions*/}
                {/*actions={['ביטול', 'אישור']}*/}
                {/*options={{ ok: { disabled: true } }}*/}
                {/*onActionPress={() => {}}*/}
                {/*/>*/}
                {/*</Dialog.Actions>*/}
                {/*</Dialog>*/}
                {/*</View>*/}
                {/*</View>*/}
                <StatusBar
                    backgroundColor="#52A3E3" // #3B4954
                    barStyle="light-content"
                />
                <Toolbar
                    style={{
                        leftElement: {
                            color: '#fff'
                        },
                        rightElement: {
                            color: '#fff'
                        },
                        titleText: {
                            color: '#fff'
                        },
                        container: {
                            elevation: 0,
                            shadowOpacity: 0,
                            backgroundColor: '#52A3E3',
                            flexDirection: 'row-reverse'
                        },
                        theme: {
                            boxShadow: 'none',
                            shadowOpacity: 0
                        }

                    }}
                    containerStyle={{
                        boxShadow: 'none',
                        elevation: 0,
                        shadowOpacity: 0
                    }}
                    leftElement="arrow-forward"
                    onLeftElementPress={() => this.props.navigation.goBack(null)}
                    centerElement="שירות"
                    onRightElementPress={this.onRightElementPress}
                    rightElement={{
                        actions: rightElementActions,
                        // menu: { labels: ['Item 1', 'Item 2'] },
                    }}
                />


                <Card row
                      enableShadow={false}
                      style={{
                          boxShadow: 'none',
                          marginBottom: 0,
                          borderRadius: 0,
                          backgroundColor: '#52A3E3'
                      }}
                      containerStyle={{
                          backgroundColor:'red'
                      }}
                      onPress={() => {
                      }}
                      enableBlur>
                    <Card.Section body>
                        <Card.Section style={{ justifyContent: 'flex-end'}}>
                            <Text text40 dark10 style={{color: 'white'}}>
                                {(service && service.display_name) || 'הכנס שם שירות'}
                            </Text>
                        </Card.Section>
                    </Card.Section>
                </Card>

                <TabBar
                    selectedIndex={1}
                >
                    <TabBar.Item onPress={() => {
                        this.setState({currentTab: 'StaffList'})
                    }}>
                        <Text>צוות</Text>
                    </TabBar.Item>

                    <TabBar.Item onPress={() => {
                        this.setState({currentTab: 'Details'})
                    }}>
                        <Text>פרטים</Text>
                    </TabBar.Item>
                </TabBar>

                <ScrollView showsVerticalScrollIndicator={false}>

                    <ToggleDisplay hide={this.state.currentTab !== 'Details'}
                                   flex
                                   style={{
                                       paddingLeft: 20,
                                       paddingRight: 20,
                                       paddingBottom: 30
                                   }}>
                        <Text style={{marginTop: 20, fontWeight: '700'}} text70>
                            אורך זמן השירות
                        </Text>
                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row-reverse',
                        }}>

                            <View style={{width: '45%'}}>
                                <Dropdown
                                    label='כמות'
                                    data={timeCount}
                                />
                            </View>
                            <View style={{width: '48%', marginRight: '2%'}}>
                                <Dropdown
                                    label='יחידת זמן'
                                    data={timeUnits}
                                />
                            </View>
                        </View>

                        <Text style={{marginTop: 20, fontWeight: '700'}} text70>
                            קטגוריה
                        </Text>

                        <Dropdown
                            containerStyle={{
                            }}
                            label='קטגוריה'
                            data={[
                                {value: 'תספורת'},
                                {value: 'צביעה'},
                                {value: 'שורשים'},
                                {value: 'שטיפת צבע'},
                                {value: 'תלתלים'},
                            ]}
                        />

                        <Text style={{marginTop: 20, fontWeight: '700'}} text70>
                            מחיר
                        </Text>
                        <View style={{
                            justifyContent: 'flex-start',
                            flexDirection: 'row-reverse',
                        }}>
                            <TextInput
                                style={{width: '28%', marginRight: '2%'}}
                                floatingPlaceholder
                                placeholder="מחיר"
                                titleStyle={{fontSize: 15}}
                            />
                            <View style={{
                                justifyContent: 'center',
                                flexDirection: 'column',
                            }}>
                                <Text style={{textAlign: 'left', fontSize: 20}}>₪</Text>
                            </View>
                        </View>
                        <Text style={{marginTop: 20, fontWeight: '700'}} text70>
                            זמינות
                        </Text>

                        <Checkbox
                            style={{
                                container: {
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row-reverse',
                                }
                            }}
                            label="לקוחות חדשים ורשומים"
                            value={'2'} checked={true}
                            onCheck={() => {
                            }}/>
                        <Checkbox
                            style={{
                                container: {
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row-reverse',
                                }
                            }}
                            label="לקוחות רשומים בלבד"
                            value={'1'}
                            onCheck={() => {
                            }}/>

                    </ToggleDisplay>
                </ScrollView>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <ToggleDisplay hide={this.state.currentTab !== 'StaffList'}
                                   style={{
                                       flex: 1,

                                       paddingLeft: 20,
                                       paddingRight: 20,
                                       paddingBottom: 30,
                                   }}>
                        <Text style={{fontWeight: '700',marginTop: 20,}} text70>
                            עדכון
                        </Text>
                        <Checkbox
                            style={{
                                container: {
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row-reverse',
                                }
                            }}
                            label="הוסף אנשי צוות חדשים אוטומטית"
                            value={'2'}
                            checked={true}
                            onCheck={() => {
                            }}/>

                        <Text style={{marginTop: 20, fontWeight: '700'}} text70>
                            צוות
                        </Text>
                        <Checkbox
                            style={{
                                container: {
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row-reverse',
                                }
                            }}
                            label="משה כהן"
                            value={'2'}
                            checked={true} onCheck={() => {
                        }}/>
                        <Checkbox
                            style={{
                                container: {
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row-reverse',
                                }
                            }}
                            label="חנה אמזלג"
                            value={'2'}
                            checked={false} onCheck={() => {
                        }}/>
                        <Checkbox
                            style={{
                                container: {
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row-reverse',
                                }
                            }}
                            label="שוקי זיקרי"
                            value={'2'}
                            checked={true} onCheck={() => {
                        }}/>
                    </ToggleDisplay>
                </ScrollView>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    image: {
        width: 54,
        height: 54,
        borderRadius: BorderRadiuses.br20,
        marginHorizontal: 14,
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
