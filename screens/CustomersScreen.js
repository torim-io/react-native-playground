import React, {Component} from 'react';
import {StyleSheet, Alert, FlatList, View, StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {ThemeManager, Colors, BorderRadiuses, ListItem, Text} from 'react-native-ui-lib'; //eslint-disable-line
import orders from '../data/orders';
import {Toolbar} from "react-native-material-ui";
import Icon from 'react-native-vector-icons/MaterialIcons';

const cardImage = require('../assets/man-312366_960_720.png');

export default class CustomersScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'לקוחות',
    };


    constructor(props) {
        super(props);

        this.state = {
            onEdit: false,
            updating: false,
        };
    }

    keyExtractor = (item, index) => item.name;

    renderRow(row, id) {
        const statusColor = '#C4C4C4';
        const cellIcon = (<Icon name="call" size={30} color="#000"/>);
        return (
            <ListItem
                activeBackgroundColor={Colors.dark60}
                activeOpacity={0.3}
                height={77.5}
                onPress={() => Alert.alert(`pressed on order #${id + 1}`)}
                animation="fadeIn"
                easing="ease-out-expo"
                duration={1000}
                style={{
                    flexDirection: 'row-reverse'
                }}
                useNativeDriver>
                <ListItem.Part middle column
                               containerStyle={
                                   [styles.border, {
                                       paddingRight: 17
                                   }]
                               }>
                    <ListItem.Part
                        containerStyle={{
                            flexDirection: 'row-reverse',
                            marginBottom: 3,
                            justifyContent: 'flex-start'

                        }}>
                        <Animatable.Image
                            // source={{uri: 'https://conceptdraw.com/a459c4/p8/preview/640/pict--young-man-ivr-people-vector-stencils-library'}}
                            source={cardImage}
                            style={styles.image}
                            animation="fadeInLeft"
                            easing="ease-out-expo"
                            duration={600}
                            delay={120}
                            useNativeDriver/>
                        <Text
                            dark10
                            text70
                            style={{marginTop: 2}}>{row.name}</Text>
                    </ListItem.Part>
                    <ListItem.Part
                        containerStyle={{
                            justifyContent: 'flex-end'

                        }}>
                        <Text text90
                              color={statusColor}
                              numberOfLines={1}
                              style={{textAlign: 'right'}}>{row.inventory.status}</Text>
                    </ListItem.Part>
                </ListItem.Part>

                <ListItem.Part>
                    {cellIcon}
                </ListItem.Part>
                <ListItem.Part>
                    <Animatable.Image
                        source={{uri: 'http://icons.iconarchive.com/icons/dtafalonso/android-l/256/WhatsApp-icon.png'}}
                        style={styles.image}
                        animation="fadeInLeft"
                        easing="ease-out-expo"
                        duration={600}
                        delay={120}
                        useNativeDriver/>
                </ListItem.Part>
            </ListItem>
        );
    }

    onRightElementPress = ({action, index}) => {
        console.log(action, index);
    }

    render() {
        return (
            <View>
                <StatusBar
                    backgroundColor="#9CABBA" // #3B4954
                    barStyle="light-content"
                />

                <Toolbar
                    style={{
                        leftElement: {
                            color: '#fff'
                        },
                        rightElement: {
                            color: '#fff'
                        },
                        container: {
                            elevation: 0,
                            backgroundColor: '#9CABBA',
                            shadowColor: 'transparent',
                            flexDirection: 'row-reverse'
                        }
                    }}

                    leftElement="menu"
                    centerElement="לקוחות"
                    onLeftElementPress={() => this.props.navigation.openDrawer()}
                    searchable={{
                        autoFocus: true,
                        placeholder: 'חפש לקוח',
                    }}
                    onRightElementPress={this.onRightElementPress}
                    rightElement={{
                        actions: ['add', 'date-range'],
                        // menu: { labels: ['Item 1', 'Item 2'] },
                    }}

                />
                <FlatList
                    data={orders}
                    renderItem={({item, index}) => this.renderRow(item, index)}
                    keyExtractor={this.keyExtractor}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 35,
        height: 35,
        borderRadius: 100,
        marginLeft: 10
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
