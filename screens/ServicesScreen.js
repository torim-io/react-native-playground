import React, {Component} from 'react';
import {StyleSheet, FlatList, View, StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {ThemeManager, Colors, BorderRadiuses, ListItem, Text} from 'react-native-ui-lib'; //eslint-disable-line
import services from '../data/services';
import {Toolbar} from "react-native-material-ui";

export default class ServicesScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'שירותים',
    };

    constructor(props) {
        super(props);

        this.state = {
            onEdit: false,
            updating: false,
        };
    }

    keyExtractor = (service, index) => service.display_name;

    renderRow(service, index) {
        const statusColor = Colors.green30;
        const {navigation} = this.props;
        return (
            <ListItem
                activeBackgroundColor={Colors.dark60}
                activeOpacity={0.3}
                height={77.5}
                onPress={() => navigation.navigate('ServiceScreen', {
                    service,
                    index,
                })}
                animation="fadeIn"
                easing="ease-out-expo"
                duration={1000}
                useNativeDriver>
                <ListItem.Part middle column containerStyle={[styles.border, {padding: 17}]}>
                    <ListItem.Part
                        containerStyle={{
                            marginBottom: 3,
                            flexDirection: 'row-reverse'
                        }}>
                        <Text dark10
                              text70
                              style={{
                                  marginRight: 10
                              }}
                              numberOfLines={1}>{service.display_name}</Text>
                        <Text
                            text70
                            style={{
                                marginTop: 2,
                            }}>₪{service.price}</Text>
                    </ListItem.Part>
                    <ListItem.Part
                        containerStyle={{
                            marginBottom: 3,
                            flexDirection: 'row-reverse'
                        }}>
                        <Text
                            text90 dark40
                            numberOfLines={1}
                            color={'#52A3E3'}
                            style={{
                                marginRight: 10,
                                textAlign: 'left',
                            }}>{service.users.reduce((acc, user) => `${acc}${acc !== '' ? ',' : ''}${user.display_name}`, '')}.</Text>
                        <Text text90
                              color={'#52A3E3'}
                              numberOfLines={1}>{service.duration} דקות</Text>
                    </ListItem.Part>
                </ListItem.Part>
            </ListItem>

        );
    }

    onRightElementPress = ({action, index}) => {
        const {navigation} = this.props;
        if (action === 'add') {
            navigation.navigate('ServiceScreen');
        }
    };

    render() {

        return (
            <View>
                <StatusBar
                    backgroundColor="#9CABBA" // #3B4954
                    barStyle="light-content"
                />
                <Toolbar
                    style={{
                        leftElement: {
                            color: '#fff'
                        },
                        rightElement: {
                            color: '#fff'
                        },
                        titleText: {
                            color: '#fff'
                        },
                        container: {
                            elevation: 0,
                            backgroundColor: '#9CABBA',
                            shadowColor: 'transparent',
                            flexDirection: 'row-reverse'
                        }
                    }}
                    leftElement="menu"
                    centerElement="שירותים"
                    onLeftElementPress={() => this.props.navigation.openDrawer()}
                    searchable={{
                        autoFocus: true,
                        placeholder: 'חפש',
                    }}
                    onRightElementPress={this.onRightElementPress}
                    rightElement={{
                        actions: ['add'],
                        // menu: { labels: ['Item 1', 'Item 2'] },
                    }}
                />
                <FlatList
                    data={services}
                    renderItem={({item, index}) => this.renderRow(item, index)}
                    keyExtractor={this.keyExtractor}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    image: {
        width: 54,
        height: 54,
        borderRadius: BorderRadiuses.br20,
        marginHorizontal: 14,
    },
    border: {
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
    container: {
        paddingTop: 0,
    },
});
