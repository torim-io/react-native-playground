import React from "react";
import {StyleSheet, Text, View} from 'react-native';

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'עמוד הבית',
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize:40}}>Home Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
