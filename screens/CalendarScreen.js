import React from "react";
import {StyleSheet, Text, View} from 'react-native';

export default class CalendarScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'לוח עבודה',
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize:40}}>לוח עבודה</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
