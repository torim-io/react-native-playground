import React, {Component} from 'react';
import {
    View, Text, ScrollView, ViewPagerAndroid
} from 'react-native';

export default class Pager extends Component {

    constructor(){
        super();

        setTimeout(()=>this.viewpager.setPage(2),1500);
    }

    render() {
        return (
            <ViewPagerAndroid
                ref={(viewpager) => {this.viewpager = viewpager}}
                style={styles.viewPager}
                initialPage={0}>
                <View style={styles.pageStyle} key="1">
                    <Text>First page</Text>
                </View>
                <View style={styles.pageStyle} key="2">
                    <Text>Second page</Text>
                </View>
            </ViewPagerAndroid>
        );
    }
}

var styles = {
    viewPager: {
        flex: 1
    },
    pageStyle: {
        alignItems: 'center',
        padding: 20,
    }
}
