import React, {Component} from 'react';
import {
    View, Text,ScrollView
} from 'react-native';

export default class Table extends Component {
    renderRow(datum) {
        return (
            <View key={datum} style={{flex: 1, alignSelf: 'stretch', flexDirection: 'row'}}>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
                <View style={{flex: 1, alignSelf: 'stretch', borderWidth: 1}}/>
            </View>
        );
    }

    render() {
        let hours = [];
        let cells = [];

        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }

        for (let i = 0; i <= 24; i++) {
            hours.push(
                (
                    <View key={i} style={{ alignItems: 'flex-end', justifyContent: 'flex-start', height: 100,borderTopWidth: 1}}>
                        <Text>{i < 10 ? '0' + i : i}:00</Text>
                    </View>
                )
            );
            cells.push(
                (
                    <View key={i} style={{ alignItems: 'flex-end',  height: 100,borderTopWidth: 1}}>
                        <Text style={{backgroundColor: 'yellow',padding:10,position: 'absolute',top:getRandomArbitrary(0,100), borderWidth:1}}>cell {i}</Text>
                    </View>
                )
            )

        }
        return (
            <ScrollView>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row-reverse'}}>
                    <View style={{flex: 1,  justifyContent: 'center', flexDirection: 'column', maxWidth:45}}>
                        {hours}
                    </View>

                    <View style={{flex:1,justifyContent: 'center', flexDirection: 'column',width:40,borderRightWidth: 1,backgroundColor:'red', position: 'relative'}}>
                        {cells}
                    </View>
                </View>
            </ScrollView>
        );
    }
}
