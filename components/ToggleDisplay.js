import View from "react-native-ui-lib/typings/components/view";
import React from "react";

const ToggleDisplay = (props) => {
    const {children, hide, style} = props;
    if (hide) {
        return null;
    }


    return (
        <View {...this.props} style={[{backgroundColor:'#fff'},style]}>
            {children}
        </View>
    );
};

export default ToggleDisplay;
