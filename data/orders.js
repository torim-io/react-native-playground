const orders = [
    {
        "name": "משה כהן",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "9:30AM צבע לשיער ארוך, עם אבי",
            "quantity": 1
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "רותי כהן",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "9:30AM צבע לשיער ארוך, עם אבי",
            "quantity": 2
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "רועי יאיר",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "9:30AM חפיפה",
            "quantity": 1
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "דרור שובל",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "מחר, גוונים בשיער ארוך, עם ריקי",
            "quantity": 0
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "דנה טקוני",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 3
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "שולי רנד",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 0
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "צביקה פיק",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 10
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "סמי מהמכולת",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 11
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "גילי גילעדי",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 10
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "נתי טובול",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 2
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "סמי עופר",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 8
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    },
    {
        "name": "שלומי סממה",
        "formattedPrice": "$19.99",
        "inventory": {
            "trackingMethod": "status",
            "status": "",
            "quantity": 8
        },
        "mediaUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuSFoiW1Ls9lduV3x5ZWy1dkmJnXB_qAk8AmFHUaeuDdEOMSr5"
    }
];

export default orders;
