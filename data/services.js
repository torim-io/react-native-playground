const services = [
    {
        display_name:'צבע לשיער',
        price:'50',
        duration:'40',
        users:[
            {
                display_name:'משה'
            },
            {
                display_name:'רינה'
            },
            {
                display_name:'סיגלית'
            },

        ]
    },
    {
        display_name:'פאדיקור',
        price:'120',
        duration:'90',
        users:[
            {
                display_name:'רינה'
            },
            {
                display_name:'סיגלית'
            },

        ]
    },
    {
        display_name:'פן לשיער קצר',
        price:'300',
        duration:'20',
        users:[
            {
                display_name:'משה'
            },
            {
                display_name:'שולמית'
            },
            {
                display_name:'רינה'
            },
            {
                display_name:'סיגלית'
            },

        ]
    },
    {
        display_name:'מחליק לשיער',
        price:'130',
        duration:'60',
        users:[
            {
                display_name:'רינה'
            },
        ]
    },

];

export default services;
