import React from 'react';
import {Alert, FlatList, StyleSheet, View, I18nManager} from 'react-native';
import {createDrawerNavigator, createStackNavigator} from 'react-navigation';
import {ThemeManager, Text,Card} from 'react-native-ui-lib'; //eslint-disable-line


import EmployeeScreen from './screens/EmployeeScreen';
import CustomersScreen from "./screens/CustomersScreen";
import TodayScreen from "./screens/TodayScreen";
import ServicesScreen from "./screens/ServicesScreen";
import ServiceScreen from "./screens/ServiceScreen";
import Demo from "./screens/Demi";
import EmployeesScreen from "./screens/EmployeesScreen";
import * as Animatable from "react-native-animatable";
import Menu from "./navigation/nav";
import ScrollableHeader from "./screens/ScrollableHeader";
import AgendaScreenWix from "./components/calendar";
import Table from "./components/Table";
import Pager from "./components/pager";

// console.log(I18nManager.forceRTL.toString());
// I18nManager.allowRTL(false);



class DrawerMenu extends React.Component {
    render() {
        const cardImage = require('./assets/man-312366_960_720.png');
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex:1}}>
                <View style={[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
                    <Card
                        style={{width: '100%', marginBottom: 0, borderRadius: 0, backgroundColor: '#9CABBA'}}
                        onPress={() => {
                        }}
                        enableBlur>

                        <Card.Section body style={{width: 90}}>
                            <Animatable.Image
                                source={cardImage}
                                style={styles.image}
                                animation="fadeInLeft"
                                easing="ease-out-expo"
                                duration={600}
                                delay={70}
                                useNativeDriver/>
                        </Card.Section>

                        <Card.Section style={{paddingLeft: 25}}>
                            <Text text40 dark10 style={{color: 'white', textAlign: 'left'}}>
                                רוני שומר
                            </Text>
                        </Card.Section>
                    </Card>
                    <View style={{width: '100%', flex: 1}}>
                        <Menu navigate={navigate}/>
                    </View>
                </View>
            </View>
        );
    }
}


const DrawerStack = createDrawerNavigator(
    {
        TodayScreen: {
            screen: TodayScreen
        },
        EmployeesScreen: {
            screen: EmployeesScreen
        },
        ServicesScreen: {
            screen: ServicesScreen
        },

        CustomersScreen: {
            screen: CustomersScreen
        },
        Demo: {
            screen: Demo
        },
        ScrollableHeader: {
            screen: ScrollableHeader
        },
        AgendaScreenWix: {
            screen: AgendaScreenWix
        },
        Table: {
            screen: Table
        },
        Pager: {
            screen: Pager
        },
    }, {

        initialRouteName: 'TodayScreen',
        contentComponent: DrawerMenu,
        drawerPosition: 'right'
    }
);




const PrimaryNav = createStackNavigator({
    EmployeeScreen: {
        screen: EmployeeScreen
    },
    ServiceScreen: {
        screen: ServiceScreen
    },
    DrawerStack: {
        screen: DrawerStack
    }
}, {
    // Default config for all screens
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'DrawerStack'
})


export default class App extends React.Component {

    render() {
        return <PrimaryNav style={styles.container}/>;
    }
}

// const App = createAppContainer(MyDrawerNavigator);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 50,
        marginHorizontal: 14,
        marginLeft: 0
    },
    border: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: ThemeManager.dividerColor,
    },
});
